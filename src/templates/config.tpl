{include file="admin/_head.tpl" title="Configuration — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}

{form_errors}

{if $ok && !$form->hasErrors()}
    <p class="confirm">
        La configuration a bien été enregistrée.
    </p>
{/if}


<form method="post" action="{$self_url}">

&nbsp;<br>

                
                <input type="checkbox" id="message_actif" name="message_actif" value="message_actif" {if $plugin.config.message_actif == true}checked{/if}>
                <label for="message_actif">Message de bienvenue activé.
                </label>
<br>&nbsp;<br>
    <fieldset>
        <legend>Configuration du message de bienvenue pour les nouveaux membres :</legend>
        <dl>

            	<dt>Objet du message de bienvenue : </dt>
		<dd><input type="text" size="60" name="objet_message" value="{$plugin.config.objet_message}"></input></dd>
		
		<dt>Contenu du message de bienvenue : </dt>
		<dd><textarea id="message" name="message" cols="50" rows="25">{$plugin.config.message}</textarea></dd>


        </dl>
    </fieldset>

    <p class="submit">
        <input type="submit" name="save" value="Enregistrer &rarr;" />
    </p>
</form>

{include file="admin/_foot.tpl"}
