<?php

namespace Garradin\Plugin\welcome;

use Garradin\Membres;
use Garradin\Users\Emails;
use Garradin\UserException;
use Garradin\Plugin;

class Welcome
{

	static public function nouveauMembre(array &$params)
	{

		$plugin = new Plugin('welcome');

		$message_actif = $plugin->getConfig('message_actif');

		if ($message_actif == true) {

			$objet_message = $plugin->getConfig('objet_message');
			$message = $plugin->getConfig('message');

			$membre = new Membres;

			$nouveau_membre = $membre->get($params['id']);

			try {		
				Emails::queue(Emails::CONTEXT_PRIVATE, array($nouveau_membre->{'email'} => $nouveau_membre), null, $objet_message, $message);
				
			}
			catch (UserException $e)
			{
				$form->addError($e->getMessage());   
			}
		}

		return false;
		
	}


}
