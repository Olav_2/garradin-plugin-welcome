<?php

namespace Garradin;

$session->requireAccess($session::SECTION_CONFIG, $session::ACCESS_ADMIN);


if (f('save'))
{

	try {
		$plugin->setConfig('message_actif', (bool) f('message_actif'));
		$plugin->setConfig('objet_message', f('objet_message'));
		$plugin->setConfig('message', f('message'));


		Utils::redirect(utils::plugin_url() . 'config.php?ok');
	}
	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}

}

$tpl->assign('ok', qg('ok') !== null);

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');
