# Plugin welcome (bienvenue !)
Cette extension envoie un email de bienvenue à chaque membre nouvellement créé.

# Installation
- Télécharger le fichier welcome.tar.gz et l'ajouter dans le répértoire garradin/plugins.
- Installer le plugin via le menu "Configuration > Extensions" de Garradin.

# Utilisation
- L'envoi de mail est effectué automatiquement dès la validation de l'ajout d'un membre.

# Paramètrage
Un écran de paramètrage accessible uniquement aux membres ayant le droit de modifier la configuration permet :
- d'activer ou désactiver l'envoi d'email de bienvenue,
- de modifier l'objet et le contenu du message de bienvenue.

Accès via Configuration > Extensions > Welcome > Configurer


